﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoBack : MonoBehaviour {

	public void GoToMenu()
    {
        GameManager.CurrentLevel++;
        GameManager.CurrentLevel = Mathf.Clamp(GameManager.CurrentLevel, 0, 8);
        SceneManager.LoadScene(1);
    }
}
