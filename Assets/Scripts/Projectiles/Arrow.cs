﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : Projectile {
    public float Damage { get { return damage; } set { damage = value; } }

    private void Awake()
    {
        StartCoroutine(base.KYS(2f));
    }

    private void FixedUpdate()
    {
        transform.position += transform.right*2;
    }
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
    }
}
