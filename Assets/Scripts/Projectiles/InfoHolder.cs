﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoHolder : MonoBehaviour {
    public PlayerControl parent;

    public void SetDamage(float damage)
    {
       if(GetComponent<Fire>() != null)
        {
            GetComponent<Fire>().Damage = damage;
        }
        if (GetComponent<Arrow>() != null)
        {
            GetComponent<Arrow>().Damage = damage;

        }
        if (GetComponent<MudBall>() != null)
        {
            GetComponent<MudBall>().Damage = damage;
        }
    }
}
