﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : Projectile
{
    public Sprite[] sprites;

    public int firstSpriteID;
    private int curSpriteID;
    public int lastSpriteID;
    public  float SecsPerFrame = 0.25f;
    InfoHolder holder;

    public float Damage { get { return damage; } set { damage = value; } }
    private void Awake()
    {
        holder = gameObject.GetComponent<InfoHolder>();
        curSpriteID = firstSpriteID = 0;
        lastSpriteID = sprites.Length - 1;
        PlayAnimation(0, SecsPerFrame);
        Debug.Log(damage);

        StartCoroutine(base.KYS(0.5f));
    }

    public void PlayAnimation(int ID, float secPerFrame)
    {
        SecsPerFrame = secPerFrame;
        StopCoroutine("AnimateSprite");
        switch (ID)
        {
            default:
                curSpriteID = firstSpriteID;
                StartCoroutine("AnimateSprite", ID);
                break;
            case 3:
                curSpriteID = firstSpriteID;
                StartCoroutine("AnimateSprite", ID);
                break;
        }
    }
    private void FixedUpdate()
    {
        transform.position = holder.parent.gameObject.transform.position + transform.right * 2 + transform.up * 5;
    }
    IEnumerator AnimateSprite(int ID)
    {
        switch (ID)
        {
            default:
                yield return new WaitForSeconds(SecsPerFrame);
                gameObject.GetComponent<SpriteRenderer>().sprite
                = sprites[curSpriteID];
                curSpriteID++;
                if (curSpriteID > lastSpriteID)
                {
                    curSpriteID = firstSpriteID;
                }
                StartCoroutine("AnimateSprite", ID);
                break;

            case 7:
                yield return new WaitForSeconds(SecsPerFrame);
                gameObject.GetComponent<SpriteRenderer>().sprite
                = sprites[curSpriteID];
                curSpriteID++;
                if (curSpriteID > lastSpriteID)
                {
                    PlayAnimation(0, 0.25f);
                }
                else
                {
                    StartCoroutine("AnimateSprite", ID);
                }
                break;
        }
    }
}
