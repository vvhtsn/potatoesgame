﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    protected float damage;
    // Use this for initialization
    protected void Start()
    {
        //damage = 10;
        //if (gameObject.GetComponent<InfoHolder>().parent.player != null)
        //    damage = gameObject.GetComponent<InfoHolder>().parent.player.Strength * 2;
        //else
        //    damage = 10;
        //Debug.Log("damage: " + damage);

    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        collision.gameObject.GetComponent<Enemy>().Health -= damage;
        Debug.Log(collision.gameObject.GetComponent<Enemy>().Health);
    }

    protected IEnumerator KYS(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
}
