﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class changeCostume : MonoBehaviour {
    public List<Sprite> items = new List<Sprite>();
    public Image image;
    public int i = 0;
    public CreateNewCharacter classButton;
    public Text costume;

    Vector3 dragonPosition, archerPosition;
    Vector3 dragonScale, archerScale;
    private void Start()
    {
        //positions for the costume renderer
        dragonPosition = new Vector3(-450, -12f);
        archerPosition = new Vector3(-320, 74, 0);

        //scales required for the image to look right
        dragonScale = new Vector3(3, 2.75f, 1);
        archerScale = new Vector3(5.98f, 4.49f, 1);
    }
    public void Change()
    {
        Color colour = image.color;
        Debug.Log(i);
        if (i >= items.Count || i == 0)
        {
            i = 0;
            colour.a = 0f;           
        }
        else
        {
            colour.a = 1f;           
        }

        //positions and scales needed for the items to look right
        if(items[i].name.Contains("Dragon"))
        {
            image.transform.localPosition = dragonPosition;
            image.transform.localScale = dragonScale;
            GameManager.party[GameManager.activePotato].Attack = "Fire";
        }
        if(items[i].name.Contains("Archer"))
        {
            image.transform.localPosition = archerPosition;
            image.transform.localScale = archerScale;
            GameManager.party[GameManager.activePotato].Attack = "Arrow";
        }
        if (items[i].name.Contains("Mage"))
        {
            image.transform.localPosition = archerPosition;
            image.transform.localScale = archerScale;
            GameManager.party[GameManager.activePotato].Attack = "Ball";
        }
        //if (items[i].name.Contains("Nothing"))
        //{
        //    GameManager.party[GameManager.activePotato].Attack = "Slash";
        //}
        image.color = colour;
        image.sprite = items[i];
        GameManager.party[GameManager.activePotato].Costume = items[i].name;
        costume.text = items[i].name;
        i++;

        
    }
}
