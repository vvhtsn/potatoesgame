﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ClickedOn : MonoBehaviour
{
    public string description;
    public int sceneNum;
    public int potatoNum;
    public changeCostume costume;
    private GameObject buttonHealth, buttonLevel, buttonArcher, buttonPotato;
    private Text descriptionText;
    // Use this for initialization
    void Start()
    {
        buttonHealth = GameObject.Find("BuyHealth");
        buttonLevel = GameObject.Find("AddMaxHealth");
        buttonArcher = GameObject.Find("BuyArcher");
        buttonPotato = GameObject.Find("OneTimePotato");
        descriptionText = GameObject.Find("UpgradeDescription"). GetComponent<Text>();

        if (GameManager.CurrentLevel < 7)
        {
            buttonHealth.SetActive(false);
            buttonLevel.SetActive(false);
            buttonArcher.SetActive(false);
            buttonPotato.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnMouseEnter()
    {
        descriptionText.text = description;
    }

    private void OnMouseExit()
    {
        descriptionText.text = "";
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneChange;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneChange;
    }

    void OnSceneChange(Scene scene, LoadSceneMode mode)
    {
        buttonHealth.SetActive(true);
        buttonLevel.SetActive(true);
        buttonArcher.SetActive(true);
        buttonPotato.SetActive(true);
    }

    private void OnMouseDown()
    {
        Debug.Log("asd");
        //ChangeScene();
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene(sceneNum);
    }
    public void ChangePotato()
    {
        GameObject Name = GameObject.Find("UI/PotatoName");

        GameManager.activePotato += potatoNum;

        Color colour = costume.image.color;
        colour.a = 0f;
        costume.image.color = colour;
        costume.i = 0;

        if (GameManager.activePotato >= 0)
        {
            if (GameManager.activePotato <= 2)
            { }
            else
                GameManager.activePotato = 0;
        }
        else
            GameManager.activePotato = 2;

        Name.GetComponent<Text>().text = GameManager.party[GameManager.activePotato].PlayerName;
    }

    public void LoadFight()
    {
        SceneManager.LoadScene(GameManager.CurrentLevel);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void BuyCastleHealth()
    {
        // Check for sufficient amount of money
        if (GameManager.currency < 10)
            return;
        // Check for max health
        if (GameManager.CastleHealth == GameManager.MaxCastleHealth)
            return;

        // Manage money
        GameManager.currency -= 10;
        GameManager.currencyTextField.text = GameManager.currency.ToString();
        // Manage health
        GameManager.CastleHealth++;
        GameManager.healthBar.value = GameManager.CastleHealth;
        

    }

    public void AddMaxHealth()
    {
        Debug.Log("addmaxhealth ");
        // Check for sufficient amount of money
        if (GameManager.currency < 100)
            return;

        // Manage money
        GameManager.currency -= 100;
        GameManager.currencyTextField.text = GameManager.currency.ToString();
        // Manage health
        GameManager.MaxCastleHealth += 5;
        GameManager.healthBar.maxValue = GameManager.MaxCastleHealth;
        GameManager.CastleHealth = GameManager.MaxCastleHealth;
        GameManager.healthBar.value = GameManager.MaxCastleHealth;
    }

    public void BuyArcher()
    {
        // Check for sufficient amount of money
        if (GameManager.currency < 50)
            return;
        // Check if castle archer already enabled
        if (GameManager.castleArcher)
            return;

        GameManager.currency -= 50;
        GameManager.currencyTextField.text = GameManager.currency.ToString();
        GameManager.castleArcher = true;
        Debug.Log("castle archer " + GameManager.castleArcher);
    }

    public void OneTimePotato()
    {
        Debug.Log("oneTimePotato");
        // Check for sufficient amount of money
        if (GameManager.currency < 100)
            return;
        // Check if oneTimePotato already enabled
        if (GameManager.oneTimePotato)
            return;

        GameManager.currency -= 100;
        GameManager.currencyTextField.text = GameManager.currency.ToString();
        GameManager.oneTimePotato = true;
    }
}
