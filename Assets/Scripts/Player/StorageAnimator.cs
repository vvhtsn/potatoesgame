﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StorageAnimator : MonoBehaviour
{
    private SpriteRenderer spr;
    public Sprite[] sprites;

    // Use this for initialization
    protected virtual void Start()
    {
        spr = GetComponent<SpriteRenderer>();

    }

    private void OnMouseEnter()
    {
        spr.sprite = sprites[1];
    }
    private void OnMouseExit()
    {
        spr.sprite = sprites[0];
    }
    private void OnMouseDown()
    {
        if(sprites[2] != null)
        {
            StartCoroutine(Fire());
        }
    }

    IEnumerator Fire()
    {
        spr.sprite = sprites[2];
        yield return new WaitForSeconds(0.5f);
        spr.sprite = sprites[1];
    }

}