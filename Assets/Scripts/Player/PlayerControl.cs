﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    // Sounds 
    AudioSource audio;
    public List<AudioClip> sounds;
    //animation stuff
    bool animate;
    public bool loop;
    public float frameSeconds = 0.1f;
    private SpriteRenderer spr;
    private Sprite[] sprites;
    private int frame = 0;
    private float deltaTime = 0;
    //
    public bool isOnTurret = false;
    private Vector3 turretPosition;
    public bool active = true;
    public int playerID;

    public GameObject projectile;
    GameObject fire;
    public GameObject bullet;

    public int minX, maxX, minY, maxY;
    public float speed = 0;

    private float horizontal, vertical;

    Passive passiveMode;
    CircleCollider2D col;

    public SpriteRenderer costumeRenderer;
    public BasePlayer player;
    public Color blinkColor;
    public PlayerSpawn MySpawner { get; set; }

    Coroutine c;
    public bool spacePressed;
    Rigidbody2D rb;
    Color colour;
    // Use this for initialization


    void Start() {
        colour = GetComponent<SpriteRenderer>().color;
        StartCoroutine(Fire());

        loop = true;
        rb = GetComponent<Rigidbody2D>();
        //animation related thingies
        spr = GetComponent<SpriteRenderer>();
        sprites = Resources.LoadAll<Sprite>("Sprites/Animations/PotatoWalk/");

        player = GameManager.party[playerID];
        if (player.Attack != null)
        {
            projectile = Resources.Load<GameObject>("Prefabs/Projectiles/" + player.Attack);
        }
       
        if (player.Costume != null && !player.Costume.Contains("base"))
            costumeRenderer.sprite = Resources.Load<Sprite>("Sprites/" + player.Costume);

        AudioSource source = gameObject.AddComponent<AudioSource>();
        source.playOnAwake = false;
    }

    // Update is called once per frame
    void Update() {
        

        if (active == true)
        {
            //animation
            if (animate)
            {
                //Keep track of the time that has passed
                deltaTime += Time.deltaTime;

                /*Loop to allow for multiple sprite frame 
                 jumps in a single update call if needed
                 Useful if frameSeconds is very small*/
                while (deltaTime >= frameSeconds)
                {
                    deltaTime -= frameSeconds;
                    frame++;
                    if (loop)
                        frame %= sprites.Length;
                    //Max limit
                    else if (frame >= sprites.Length)
                        frame = sprites.Length - 1;
                }
                //Animate sprite with selected frame
                spr.sprite = sprites[frame];
            }

            if (Input.GetKey("space"))
            {
                spacePressed = true;
            }
            else
            {
                spacePressed = false;
            }
            

            // get input
            horizontal = Input.GetAxis("Horizontal") * speed;
            vertical = Input.GetAxis("Vertical") * speed;

            if (horizontal != 0 || vertical != 0) animate = true;
            else animate = false;
            
            
            
            // turn the player
            if (horizontal < 0)
                transform.eulerAngles = new Vector3(0, 180, 0);
            if (horizontal > 0)
                transform.eulerAngles = new Vector3(0, 0, 0);

            // calculate new point
            horizontal += transform.position.x;
            vertical += transform.position.y;

            // make sure not to reach the edge of the map
            horizontal = Mathf.Clamp(horizontal, minX, maxX);
            vertical = Mathf.Clamp(vertical, minY, maxY);

            transform.position = new Vector3(horizontal, vertical, 0);
            //rb.AddForce(new Vector2(horizontal, vertical));
            //turn the tower mode on and off
            if (Input.GetKeyDown("f"))
            {
                MySpawner.ChangePotato(playerID);
            }
        }
    }
   

   public void TowerModeOn()
    {
        passiveMode = gameObject.AddComponent<Passive>();
        if (!GetComponent<CircleCollider2D>())
            col = gameObject.AddComponent<CircleCollider2D>();
        if(isOnTurret)
        {
            transform.position = turretPosition;
            col.radius = 75;
        }
        else
        {
            col.radius = 50;
        }
        
        col.isTrigger = true;
        gameObject.layer = 11;
        passiveMode.Bullet = bullet;
    }

    public void TowerModeOff()
    {    
        Destroy(passiveMode);
        Destroy(gameObject.GetComponent<DrawCircle>());
        Destroy(gameObject.GetComponent<LineRenderer>());

        col.radius = 6;
        gameObject.layer = 12;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 13)
        {
            isOnTurret = true;
            turretPosition = other.transform.position;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            StartCoroutine(KnockBack(collision.transform.position));
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.layer == 13)
        {
            isOnTurret = false;
        }
    }

    IEnumerator KnockBack(Vector3 enemyPos)
    {
        active = false;

        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = sounds[Random.Range(0, sounds.Capacity)];
        audio.PlayOneShot(audio.clip);
        Rigidbody2D rig = GetComponent<Rigidbody2D>();
        rig.velocity = Vector3.zero;
        rig.angularVelocity = 0;
        Vector2 force = (transform.position - enemyPos).normalized * 5000;
        rig.AddForce(force);
        Debug.Log("knock");
        yield return new WaitForSeconds(0.3f);
        rig.velocity = Vector3.zero;
        rig.angularVelocity = 0;

        active = true;
    }

    IEnumerator Fire()
    {
        yield return new WaitUntil(() => spacePressed == true);
        float damage = 5;
        c = StartCoroutine(Blink());
        while (spacePressed)
        {
            damage += 0.05f;
            yield return null;
        }
        StopCoroutine(c);
        GetComponent<SpriteRenderer>().color = colour;

        if (fire == null || !fire.name.Contains("Fire"))
        {
            fire = Instantiate(projectile, transform.position + transform.right * 10, Quaternion.identity);
            fire.transform.localRotation = transform.rotation;
            if (projectile.name.Contains("Fire"))
            { fire.transform.localScale = new Vector3(3, 3, 1); }
            fire.GetComponent<InfoHolder>().parent = this;
            fire.GetComponent<InfoHolder>().SetDamage(damage);
        }
        StartCoroutine(Fire());
    }

    IEnumerator Blink()
    {
        while (true)
        {           
            GetComponent<SpriteRenderer>().color = blinkColor;
            yield return new WaitForSeconds(0.4f);
            GetComponent<SpriteRenderer>().color = colour;
            yield return new WaitForSeconds(0.4f);
        }
    }
}
