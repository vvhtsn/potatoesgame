﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePlayer : MonoBehaviour {

    private string playerName;
    private int playerLvll;
    private BaseClass playerclass;
    private int strength;
    private int agility;
    private int intellect;
    private string costume;
    public string Attack { get; set; }



    public string Costume
    {
        get { return costume; }
        set { costume = value; }
    }

    public string PlayerName
    {
        get { return playerName; }
        set { playerName = value; }
    }
    public int PlayerLvl
    {
        get { return playerLvll; }
        set { playerLvll = value; }
    }

    public BaseClass Playerclass
    {
        get { return playerclass; }
        set { playerclass = value; }
    }

    public int Strength
    {
        get { return strength; }
        set { strength = value; }
    }
    public int Agility
    {
        get { return agility; }
        set { agility = value; }
    }
    public int Intellect
    {
        get { return intellect; }
        set { intellect = value; }
    }

}
