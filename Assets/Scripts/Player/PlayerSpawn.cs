﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour {
    public GameObject player;
    public GameObject[] spawnPoints = new GameObject[3];
    bool coolDown;
    int numberOfPotats = 0;      
    public GameObject[] potatoes;
	// Use this for initialization
	void Start ()
    {
        switch(GameManager.CurrentLevel)
        {
            case 4: numberOfPotats = 1;
                break;

            case 5: numberOfPotats = 2;
                break;

            case 6: numberOfPotats = 3;
                break;

            case 7: numberOfPotats = GameManager.oneTimePotato == true ? 4 : 3;
                GameManager.oneTimePotato = false;
                break;

            case 8: numberOfPotats = GameManager.oneTimePotato == true ? 4 : 3;
                GameManager.oneTimePotato = false;
                break;
        }
        potatoes = new GameObject[numberOfPotats];
        for(int i = 0; i < potatoes.Length; i++)
        {
            // Set up potatoes as towers
            var playa = Instantiate(player, spawnPoints[i].transform.position, Quaternion.identity);
            potatoes[i] = playa;
            playa.GetComponent<PlayerControl>().MySpawner = this;
            playa.GetComponent<PlayerControl>().playerID = i;
            playa.GetComponent<PlayerControl>().player = GameManager.party[i];
            playa.GetComponent<PlayerControl>().active = false;
            playa.GetComponent<PlayerControl>().TowerModeOn();
            // Set up first potato as player
            potatoes[0].GetComponent<PlayerControl>().active = true;
            potatoes[0].GetComponent<PlayerControl>().TowerModeOff();
            Camera.main.gameObject.GetComponent<CameraFollow>().player = potatoes[0].transform;
        }

       

        if (GameManager.castleArcher)
        {
            var archer = GameObject.Find("CastleArcher");
            var col = archer.AddComponent<CircleCollider2D>();
            col.radius = 50;
            archer.AddComponent<Passive>();
            col.isTrigger = true;
            gameObject.layer = 11;
            archer.GetComponent<Passive>().Bullet = potatoes[0].GetComponent<Passive>().Bullet;
            archer.GetComponent<SpriteRenderer>().enabled = true;        
        }

        if (GameManager.oneTimePotato)
        {
            // Disable oneTimePotato
            GameManager.oneTimePotato = false;           
        }
    }
	

	public void ChangePotato(int num)
    {
        if (coolDown == false)
        {
            coolDown = true;
            potatoes[num].GetComponent<PlayerControl>().active = false;
            potatoes[num].GetComponent<PlayerControl>().TowerModeOn();
            if (num + 1 < potatoes.Length)
            {
                potatoes[num + 1].GetComponent<PlayerControl>().active = true;
                potatoes[num + 1].GetComponent<PlayerControl>().TowerModeOff();
                Camera.main.gameObject.GetComponent<CameraFollow>().player = potatoes[num + 1].transform;
            }
            else
            {
                potatoes[0].GetComponent<PlayerControl>().active = true;
                potatoes[0].GetComponent<PlayerControl>().TowerModeOff();
                Camera.main.gameObject.GetComponent<CameraFollow>().player = potatoes[0].transform;
            }
            StartCoroutine(CoolDown());
        }

    }

    IEnumerator CoolDown()
    {
        yield return new WaitForSeconds(1);
        coolDown = false;
    }
}
