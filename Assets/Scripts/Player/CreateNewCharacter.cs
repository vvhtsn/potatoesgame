﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateNewCharacter : MonoBehaviour {

    //private BasePlayer player;
    public BasePlayer player;
    private bool isMage;
    GameManager gm;
    public Text Class;

    public string costume;
    private void Start()
    {

    }
    //private void OnGUI()
    //{
    //    if (GUILayout.Toggle(isMage, "Mage Class"))
    //    {
    //        isMage = true;
    //        isWarrior = false;
    //    }

    //    if (GUILayout.Toggle(isWarrior, "Warrior Class"))
    //    {
    //        isWarrior = true;
    //        isMage = false;
    //    }

    //    if(GUILayout.Button("Create"))
    //    {
    //      if(isMage)
    //        {
    //            player.Playerclass = new Mage();
    //        }
    //      else if(isWarrior)
    //        {
    //            player.Playerclass = new Warrior();
    //        }
    //        //player.PlayerLvl = 1;
    //        player.Strength = player.Playerclass.Strength;
    //        player.Agility = player.Playerclass.Agility;
    //        player.Intellect = player.Playerclass.Intellect;

    //        Debug.Log(player.Playerclass.ToString() + player.PlayerLvl + player.Strength + player.Agility + player.Intellect);
    //    }
    //}

    public void ChangeClass()
    {

        
        player = new BasePlayer();

        if (!isMage)
        {
            player.Playerclass = new Mage();
            isMage = true;
        }
        else
        {
            player.Playerclass = new Warrior();
            isMage = false;
        }
        //player.PlayerLvl = 1;
        player.Strength = player.Playerclass.Strength;
        player.Agility = player.Playerclass.Agility;
        player.Intellect = player.Playerclass.Intellect;
        GameManager.party[GameManager.activePotato] = player;

        Class.text = player.Playerclass.ToString();
    }
}
