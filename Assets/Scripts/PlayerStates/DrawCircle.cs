﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class DrawCircle : MonoBehaviour {
    [Range(0, 50)]
    public int segments = 50;
    [Range(0, 5)]
    public float xradius = 5;
    [Range(0, 5)]
    public float yradius = 5;
    LineRenderer line;

    void Start()
    {
        line = gameObject.GetComponent<LineRenderer>();
        var radius = gameObject.GetComponent<CircleCollider2D>().radius;
        xradius = yradius = radius;
        line.positionCount = (segments + 1);
        line.useWorldSpace = false;
        line.material = new Material(Shader.Find("Diffuse"));
        line.startWidth = 0.4f;
        CreatePoints();
    }

    void CreatePoints()
    {
        float x;
        float y;
        float z;

        float angle = 20f;

        for (int i = 0; i < (segments + 1); i++)
        {
            x = Mathf.Sin(Mathf.Deg2Rad * angle) * xradius;
            z = Mathf.Cos(Mathf.Deg2Rad * angle) * yradius;

            if (gameObject.transform.rotation.y > 0.5f)
            {
                line.SetPosition(i, new Vector3(x, z, 4));
            }

            else
            {
                line.SetPosition(i, new Vector3(x, z, -4));
            }

            angle += (360f / segments);
        }
    }
}


