﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public Vector3 target;
    public float speed;
    public float damage { get; set; }
   

    void FixedUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, target, speed);
        damage = 2;
        if(transform.position == target)
        {
            StartCoroutine(KYS());
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            collision.gameObject.GetComponent<Enemy>().Health -= damage;
            Destroy(gameObject);
        }
    }
    
    private void OnTriggerEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            collision.gameObject.GetComponent<Enemy>().Health -= damage;
            Destroy(gameObject);
        }
    }

    IEnumerator KYS()
    {
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
}
