﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Passive : MonoBehaviour {

    public float bulletSpeed = 10;
    public float shootDelay = 1;
    public GameObject Bullet;
    public List<GameObject> targets = new List<GameObject>();

    private float colliderRadius;

    PlayerControl control;
	// Use this for initialization
	void Start () {

        StartCoroutine(Circle());
        control = gameObject.GetComponent<PlayerControl>();
        colliderRadius = transform.GetComponent<CircleCollider2D>().radius;
        
        StartCoroutine(Shoot());
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		foreach(GameObject target in targets)
        {
            if (target == null)
            {
                Debug.Log(Vector3.Distance(transform.position, target.transform.position));
                targets.Remove(target);
                break;
            }
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9)    // Layer Enemies
        {
            targets.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9)    // Layer Enemies
        {
            targets.Remove(collision.gameObject);
        }
    }

    // Creates circle for some time and removes it
    IEnumerator Circle()
    {
        gameObject.AddComponent<DrawCircle>();
        yield return new WaitForSeconds(3);
        Destroy(GetComponent<DrawCircle>());
        Destroy(GetComponent<LineRenderer>());
    }

    IEnumerator Shoot()
    {
        yield return new WaitForSeconds(shootDelay);
        if (targets.Count > 0)
        {
            var bullet = Instantiate(Bullet);
            bullet.GetComponent<Bullet>().damage = (control != null ? control.player.Strength : 10) * 2;
            bullet.transform.position = transform.position;
            bullet.GetComponent<Bullet>().target = targets[0].transform.position;
            bullet.GetComponent<Bullet>().speed = bulletSpeed;
        }
        StartCoroutine(Shoot());
    }
}
