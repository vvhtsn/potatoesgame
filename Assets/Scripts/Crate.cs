﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crate : MonoBehaviour {
    public int health;
    public Color blinkColor;
    Color colour;
    // Use this for initialization
    void Start () {
        colour = GetComponent<SpriteRenderer>().color;

    }

    // Update is called once per frame
    void Update () {
		if (health <= 0)
        {
            Destroy(gameObject);
        }
	}
    
    
     public IEnumerator Blink()
        {
            while (true)
            {
                GetComponent<SpriteRenderer>().color = blinkColor;
                yield return new WaitForSeconds(0.4f);
                GetComponent<SpriteRenderer>().color = colour;
                yield return new WaitForSeconds(0.4f);
            }
        }
    
}
