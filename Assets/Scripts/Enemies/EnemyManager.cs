﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnemyManager : MonoBehaviour {
    public GameObject[] enemies = new GameObject[2];
    public GameObject endText;

    public List<GameObject> activeEnemies = new List<GameObject>();
    public List<AudioClip> winGame, loseGame;

    public GameObject spawnPoint;
    public int waveNumber = 0, numberOfWaves;

    public bool spawnEnemies = true;
    bool gameEnded;
    int totalEnemies;
    
    //// Use this for initialization
    //void Start() {
    //    waveNumber = 1;
    //    StartCoroutine(SpawnWave());
    //}

    private void Update()
    {
        // For tutorial
        if (!spawnEnemies)
            return;

        if (activeEnemies.Count <= 0)
        {
            if (waveNumber < numberOfWaves)
            { waveNumber++; StartCoroutine(SpawnWave()); }
            else
            { if(!gameEnded) WinGame(); }
        }
           
    }

    IEnumerator SpawnWave()
    {
        for (int i = 0; i < waveNumber * 2; i++)
        {
            SpawnEnemy(i);
            yield return new WaitForSeconds(1);
        }
    }
    void SpawnEnemy(int enemyID)
    {
        totalEnemies++;
        var enemy = Instantiate(enemies[Random.Range(0, enemies.Length)], spawnPoint.transform.position, Quaternion.identity, transform);
        enemy.GetComponent<Enemy>().MyManager = this;
        enemy.GetComponent<Enemy>().EnemyID = enemyID;
        activeEnemies.Add(enemy);
    }

    void WinGame()
    {
        GameManager.currency += totalEnemies * 10;
        gameEnded = true;
        spawnEnemies = false;
        endText.transform.parent.gameObject.GetComponent<Canvas>().enabled = true;
        endText.GetComponent<Text>().text = "You have won, you're a good noodel";
        AudioSource audio = gameObject.AddComponent<AudioSource>();
        audio.clip = winGame[Random.Range(0, winGame.Capacity)];
        audio.PlayOneShot(audio.clip);
    }
}
