﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {
    public List<AudioClip> enemyHit;
    public Color hitColor;
    public float step = 1;
    public Vector3 target;
    public List<Vector3> points = new List<Vector3>();
    public EnemyManager MyManager { get; set; }
    public int EnemyID { get; set; }
    public float previousHealth, health = 20;

    private AudioSource audioSource;
    private int index;
    private SpriteRenderer renderer;
    private bool attackCastleOn = false;
    
    public float Health { get { return health; } set { health = value; } }

    bool stunned;
    Slider healthUI;
	// Use this for initializatio
	void Awake ()
    {
        for (int i = 0; i < GameObject.Find("Points").transform.childCount; i++)
        {
            points.Add(GameObject.Find("Points").transform.GetChild(i).transform.position);
        }

        target = points[index];
        healthUI = GameManager.healthBar;
        healthUI.maxValue = GameManager.MaxCastleHealth;
        healthUI.value = GameManager.CastleHealth;
        previousHealth = health;
        renderer = GetComponent<SpriteRenderer>();
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(health <= 0)
        {
            MyManager.activeEnemies.Remove(gameObject);
            Destroy(gameObject);
        }
        //if (pointReached)
        //{
        //    if (index >= points.Count - 1) { 
        //        GameManager.CastleHealth--;
        //    Debug.Log(GameManager.CastleHealth); }
        //else
        //    index++;
        //    target = points[index];
        //    pointReached = false;
        //}

        //if (transform.position.x == target.x && transform.position.y == target.y)
        //    pointReached = true;
        //else
        if (!stunned)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, step);
        }
        var tempRot = transform.rotation;
        if (target.x > transform.position.x && transform.rotation. y == 0)
        {
            tempRot.y = 180;
            transform.rotation = tempRot;
        }
        if (target.x < transform.position.x && (transform.rotation.y - 180)  < 0)
        {
            tempRot.y = 0;
            transform.rotation = tempRot;
        }

        if(previousHealth != health)
        {
            previousHealth = health;
            StartCoroutine(GotHit());
        }
    }

    IEnumerator GotHit()
    {
        stunned = true;
        renderer.color = hitColor;
        audioSource.clip = enemyHit[Random.Range(0, enemyHit.Capacity)];
        audioSource.PlayOneShot(audioSource.clip);
        yield return new WaitForSeconds(1);
        renderer.color = Color.white;
        stunned = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.gameObject.name.Contains("Fire"))
            if (index >= points.Count - 1 && collision.gameObject.layer == 0)
            {
                
                StartCoroutine(AttackCastle());
                healthUI.value = GameManager.CastleHealth;
            }
            else if (collision.gameObject.layer == 10)
            {
                index++;
                index = Mathf.Clamp(index, 0, points.Count-1);
                target = points[index];
            }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Crate")
        {
            StartCoroutine(collision.gameObject.GetComponent<Crate>().Blink());
            StartCoroutine(AttackCrate(collision.gameObject));
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Crate")
        {
            stunned = false;
        }
    }
    void LoseGame()
    {
        MyManager.endText.transform.parent.gameObject.GetComponent<Canvas>().enabled = true;
        MyManager.endText.GetComponent<Text>().text = "You have lost, you're a bad noodel";
    }

    IEnumerator AttackCrate(GameObject crate)
    {
        stunned = true;
        crate.GetComponent<Crate>().health--;
        yield return new WaitForSeconds(1);
        if(crate != null)
        {
            StartCoroutine(AttackCrate(crate));
        }
    }
    IEnumerator AttackCastle()
    {
        //attackCastleOn = true;
        if (GameManager.CastleHealth <= 0)
        {
            LoseGame();
            StopAllCoroutines();
        }
        yield return new WaitForSeconds(1);
        GameManager.CastleHealth = GameManager.CastleHealth > 0 ? GameManager.CastleHealth-1 : 0;
        healthUI.value = GameManager.CastleHealth;
        StartCoroutine(AttackCastle());
        //attackCastleOn = false;
    }   
}
