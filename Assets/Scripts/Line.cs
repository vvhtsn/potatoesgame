﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class Line : MonoBehaviour {
    public Vector3 p0, p1;
    public List<Vector3> points;
    public int[] triangles;
    Mesh mesh;
    private void Start()
    {
        mesh = GetComponent<MeshFilter>().mesh;
        for (int i = 1; i <= 10 ; i++)
        {
            points.Add((p1 - p0) / i);
        }

        var temp = new List<Vector3>();
        temp.AddRange(points);
        for(int j = 0; j < points.Count - 1; j++)
        {
            Vector3 extraPoint = (points[j] + points[j + 1])/ 2;
            if (j % 2 == 0)
            {
                extraPoint.y += 2;
            }
            else
            {
                extraPoint.y -= 2;
            }
            temp.Add(extraPoint);

           
        }
        points.Clear();
        points.AddRange(temp);
        Vector3[] vertices = points.ToArray();
        Vector2[] uv = new Vector2[vertices.Length * 2];
        Vector4[] tangents = new Vector4[vertices.Length];
        Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);
              
        mesh.vertices = vertices;
        mesh.tangents = tangents;

        int[] triangles = new int[vertices.Length  * 6];

        int ti = 0, vi = 0;
        for (int x = 0; x < vertices.Length; x++, ti += 3, vi++)
        {
            if (vi + 10 + 2 < vertices.Length)
            {
                triangles[ti] = vi;
                triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                triangles[ti + 4] = triangles[ti + 1] = vi + 10 + 1;
                triangles[ti + 5] = vi + 10 + 2;
            }
            }
        
        try
        {
            mesh.triangles = triangles;
            mesh.RecalculateNormals();
        }
        catch
        {
            Debug.Break();
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        for (int i = 0; i < points.Count; i++)
        {
            Gizmos.DrawSphere(points[i], 0.1f);
        }
    }

}
