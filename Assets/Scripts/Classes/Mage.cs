﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mage : BaseClass
{

    public Mage()
    {
        CharacterClassName = "Mage";
        CharacterClassDescription = "Magical potato that uses its mystic powers to fry its enemies!";
        Strength = 5;
        Agility = 3;
        Intellect = 10;
    }
}

