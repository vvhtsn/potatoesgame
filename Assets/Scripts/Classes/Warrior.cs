﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warrior : BaseClass {

	public Warrior()
    {
        CharacterClassName = "Warrior";
        CharacterClassDescription = "Powerful potato warrior, mashing his enemies with his strength";
        Strength = 10;
        Agility = 6;
        Intellect = 5;
    }
}
