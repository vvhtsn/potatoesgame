﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static int maxCastleHealth = 30;
    private static int currentCastleHealth = 30;
    private static int currentLevel = 4;
    private static AudioSource audio;

    public static List<AudioClip> bgMusic = new List<AudioClip>();
    public static GameManager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
    public static BasePlayer[] party = new BasePlayer[4];   
    public static int activePotato;
    public static int MaxCastleHealth { get { return maxCastleHealth; } set { maxCastleHealth = value; } }
    public static int CastleHealth { get { return currentCastleHealth; } set { currentCastleHealth = value; } }
    public static int CurrentLevel { get { return currentLevel; } set { currentLevel = value; } }
    public static int currency = 0;
    public static bool castleArcher = false,
        oneTimePotato = false;
    public static Slider healthBar;
    public static Text currencyTextField;

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneChange;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneChange;
    }

    void Awake()
    {
        if (GameObject.Find("HealthBar") != null)
            healthBar = GameObject.Find("HealthBar").GetComponent<Slider>();

        if (party[0] == null)
        {
            for (int i = 0; i < party.Length; i++)
            {
                party[i] = new BasePlayer();
                party[i].Playerclass = new Warrior();
                party[i].Strength = party[i].Playerclass.Strength;
                party[i].Agility = party[i].Playerclass.Agility;
                party[i].Intellect = party[i].Playerclass.Intellect;
                party[i].PlayerName = "Potato " + i;

            }
        }

        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

    }
    private void Start()
    {
        audio = gameObject.GetComponent<AudioSource>();
        if (bgMusic.Capacity == 0)
            bgMusic = GameObject.Find("EventSystem").GetComponent<GameMusic>().BackGroundMusic.ToList();
        StartCoroutine(CheckMusic());
    }

    void OnSceneChange(Scene scene, LoadSceneMode mode)
    {
        if (GameObject.Find("HealthBar") != null)
        {
            healthBar = GameObject.Find("HealthBar").GetComponent<Slider>();
            healthBar.maxValue = maxCastleHealth;
            healthBar.value = currentCastleHealth;
        }
        if (GameObject.Find("Currency") != null)
        {
            currencyTextField = GameObject.Find("Currency").GetComponent<Text>();
            currencyTextField.text = currency.ToString();
        }

        audio.Stop();
        audio.clip = bgMusic[Random.Range(0, bgMusic.Capacity)];
        audio.PlayOneShot(audio.clip);
    }

    IEnumerator CheckMusic()
    {
        yield return new WaitWhile(()=>audio.isPlaying);
        audio.Stop();
        audio.clip = bgMusic[Random.Range(0, bgMusic.Capacity)];
        audio.PlayOneShot(audio.clip);
        StartCoroutine(CheckMusic());
    }
}
    