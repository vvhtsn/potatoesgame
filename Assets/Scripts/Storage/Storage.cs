﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Storage : MonoBehaviour {

    private GameManager gameManager;
    private Text currency; 

    private void Start()
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        currency = GameObject.Find("Currency").GetComponent<Text>();
    }

    private void Update()
    {
        currency.text = GameManager.currency.ToString();
    }

    public void CastleHealth()
    {
        if (GameManager.currency > 99)
        {
            GameManager.CastleHealth += 10;
            GameManager.currency -= 100;
            Debug.Log("currency " + GameManager.currency);
            Debug.Log("health " + GameManager.CastleHealth);
        }
    }

    public void CastleArcher()
    {
        if (GameManager.currency > 149)
        {
            GameManager.castleArcher = true;
            GameManager.currency -= 150;
            Debug.Log("currency " + GameManager.currency);
            Debug.Log("archer " + GameManager.castleArcher);
        }
    }

    public void MenuButton()
    {
        SceneManager.LoadScene(1);
    }
}
