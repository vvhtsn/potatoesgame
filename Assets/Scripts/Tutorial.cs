﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour {

    EnemyManager enemyManager;
    GameObject tutorialUI;
    PlayerSpawn playerSpawn;

	// Use this for initialization
	void Start () {
        enemyManager = GameObject.Find("Enemy Manager").GetComponent<EnemyManager>();
        tutorialUI = GameObject.Find("Tutorial");
        playerSpawn = GameObject.Find("PlayerSpawn").GetComponent<PlayerSpawn>();

        switch(GameManager.CurrentLevel)
        {
            case 4:
                enemyManager.spawnEnemies = false;
                StartCoroutine(FireTutorial());
                break;

            case 5:
                enemyManager.spawnEnemies = false;
                StartCoroutine(SwitchTutorial());
                break;
        }
	}
	
	IEnumerator FireTutorial()
    {
        yield return new WaitUntil(() => Input.GetKeyDown("space"));
        tutorialUI.SetActive(false);
        enemyManager.spawnEnemies = true;
    }

    IEnumerator SwitchTutorial()
    {
        yield return new WaitUntil(() => Input.GetKeyDown("f"));
        StartCoroutine(TurretTutorial());
    }

    IEnumerator TurretTutorial()
    {
        tutorialUI.GetComponentInChildren<Text>().text = "Come to Turret and press F";
        // Get all potatoes in game
        CircleCollider2D potato1 = playerSpawn.potatoes[0].GetComponent<CircleCollider2D>();
        CircleCollider2D potato2 = playerSpawn.potatoes[1].GetComponent<CircleCollider2D>();
        // Wait until one potato get into turret
        yield return new WaitUntil(() => 
        potato1.radius == 75 ||
        potato2.radius == 75);

        tutorialUI.SetActive(false);
        enemyManager.spawnEnemies = true;
    }
}
