﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    const int CAMERAZPOSITION = -130;

    public Transform player;
    public int XOffset, YOffset;
    public float speed;

    private int minX = -55, maxX = 55, minY = 25, maxY = 85;
    private float currentX, currentY, targetX, targetY,
        zoomOutX = 0,
        zoomOutY = 55,
        zoomOutZ = -186;
    private bool zoomOutDone = false;
    public Transform camTransform;
    Vector3 originalPos;

    // Use this for initialization
    void Start () {
        // Zoom out position
        transform.position = new Vector3(zoomOutX, zoomOutY, zoomOutZ);
        camTransform = GetComponent(typeof(Transform)) as Transform;

    }


    // Update is called once per frame
    void Update () {
        currentX = transform.position.x;
        currentY = transform.position.y;

        if (Mathf.Abs(player.position.x - currentX) > XOffset)
        {
            targetX += player.position.x - currentX;
            targetX = Mathf.Lerp(player.position.x, currentX, 0.5f);
            targetX = Mathf.Clamp(targetX, minX, maxX);

        }

        if (Mathf.Abs(player.position.y - currentY) > YOffset)
        {
            targetY += player.position.y - currentY;
            targetY = Mathf.Lerp(player.position.y, currentY, 0.1f);
            targetY = Mathf.Clamp(targetY, minY, maxY);
        }
        targetX = currentX == targetX ? currentX : targetX;
        targetY = currentY == targetY ? currentY : targetY;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(targetX, targetY, CAMERAZPOSITION), speed);
        
    }

    public void Shake(float time)
    {
        StartCoroutine(ShakeFor(time));
    }

    IEnumerator ShakeFor(float time)
    {
        yield return new WaitForSeconds(time);
        camTransform.localPosition = camTransform.localPosition + UnityEngine.Random.insideUnitSphere * 2;
    }
}
