﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
public class CreateNewWeapon : MonoBehaviour {

    private BaseWeapon newWeapon;
    private string[] weaponNames = new string[2] { "Sword of rapiness", "Literally your mum" };
    private void Start()
    {
        CreateWeapon();
    }
    private void CreateWeapon()
    {

        newWeapon = new BaseWeapon();
        newWeapon.ItemName = weaponNames[Random.Range(0, weaponNames.Length)];
        newWeapon.ItemDescription = "I mean it";

        newWeapon.ItemID = Random.Range(1, 101);

        //stats
        newWeapon.Strength = Random.Range(1, 11);
        newWeapon.Agility = Random.Range(1, 11);
        newWeapon.Intelligence = Random.Range(1, 11);

        ChooseWeapontype();

        newWeapon.SpellEffectID = Random.Range(1, 101);
    }

    void ChooseWeapontype()
    {
        int randomTemp = UnityEngine.Random.Range(1, Enum.GetValues(typeof(BaseWeapon.WeaponTypes)).Length);
        newWeapon.WeaponType = (BaseWeapon.WeaponTypes)randomTemp;

    }
}
