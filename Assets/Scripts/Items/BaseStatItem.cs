﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseStatItem : BaseItem{

    private int agility;
    private int strength;
    private int intelligence;

    
    public int Strength
    {
        get { return strength; }
        set { strength = value; }
    }
    public int Intelligence
    {
        get { return intelligence; }
        set { intelligence = value; }
    }
    public int Agility
    {
        get { return agility; }
        set { agility = value; }
    }
}
