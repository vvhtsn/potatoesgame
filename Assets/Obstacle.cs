﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public GameObject connectingPortal;
    public Color deactivated;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(gameObject.tag == "Portal")
        {
            if (Random.Range(0, 10) >= 1)
            {
                StartCoroutine(TurnOffOtherPortal());
                collision.transform.position = connectingPortal.transform.position;
            }
            else
            {
                StartCoroutine(StuckInPortal(collision.gameObject));
            }
        }
    }


    IEnumerator StuckInPortal(GameObject potato)
    {
        potato.GetComponent<PlayerControl>().active = false;
        var timeToSpin = 1.5f;
        while (timeToSpin > 0)
        {
            timeToSpin -= Time.deltaTime;
            potato.transform.Rotate(new Vector3(0, 0, 1), 500 * Time.deltaTime);
            yield return null;
        }
        potato.GetComponent<SpriteRenderer>().enabled = false;        
        yield return new WaitForSeconds(3);
        potato.GetComponent<PlayerControl>().active = true;
        potato.GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<CircleCollider2D>().enabled = false;
        yield return new WaitForSeconds(5);
        GetComponent<CircleCollider2D>().enabled = true;
    }

    IEnumerator TurnOffOtherPortal()
    {
        var colour = GetComponent<SpriteRenderer>().color;
        GetComponent<SpriteRenderer>().color = deactivated;
        connectingPortal.GetComponent<CircleCollider2D>().enabled = false;
        yield return new WaitForSeconds(5);
        connectingPortal.GetComponent<CircleCollider2D>().enabled = true;
        GetComponent<SpriteRenderer>().color = colour;
    }
}
