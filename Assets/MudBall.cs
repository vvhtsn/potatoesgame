﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MudBall : Projectile
{
    public Sprite smashed;
    public float Damage { get { return damage; } set { damage = value; } }

    private void Awake()
    {
        StartCoroutine(base.KYS(1f));
    }

    private void FixedUpdate()
    {
        transform.position += transform.right * 2;
    }
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = smashed;
        base.OnTriggerEnter2D(collision);
        StartCoroutine(Wait(0.5f));
        
    }

    private IEnumerator Wait(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
}
