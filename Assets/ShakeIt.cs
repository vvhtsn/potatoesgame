﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeIt : MonoBehaviour {

    float x, y, z;
    public List<AudioClip> coinSounds;
	// Use this for initialization
	void Start () {
        x = transform.position.x;
        y = transform.position.y;
        z = transform.position.z;

        if (GameManager.CurrentLevel > 4)
            StartCoroutine(Shake());

    }

   

    IEnumerator Shake()
    {
        AudioSource source = gameObject.AddComponent<AudioSource>();
        source.clip = coinSounds[Random.Range(0, coinSounds.Capacity-1)];
        source.PlayOneShot(source.clip);
        transform.position = new Vector3(x + Random.Range(-5, 5), y + Random.Range(-5, 5), z);
        yield return new WaitForSeconds(0.1f);
        transform.position = new Vector3(x + Random.Range(-5, 5), y + Random.Range(-5, 5), z);
        yield return new WaitForSeconds(0.1f);
        transform.position = new Vector3(x + Random.Range(-5, 5), y + Random.Range(-5, 5), z);
        yield return new WaitForSeconds(0.1f);
        transform.position = new Vector3(x + Random.Range(-5, 5), y + Random.Range(-5, 5), z);
        yield return new WaitForSeconds(0.1f);
        transform.position = new Vector3(x + Random.Range(-5, 5), y + Random.Range(-5, 5), z);
        yield return new WaitForSeconds(0.1f);
        transform.position = new Vector3(x + Random.Range(-5, 5), y + Random.Range(-5, 5), z);
        yield return new WaitForSeconds(0.1f);
        transform.position = new Vector3(x + Random.Range(-5, 5), y + Random.Range(-5, 5), z);
        yield return new WaitForSeconds(0.1f);
        transform.position = new Vector3(x + Random.Range(-5, 5), y + Random.Range(-5, 5), z);
        yield return new WaitForSeconds(0.1f);
        transform.position = new Vector3(x + Random.Range(-5, 5), y + Random.Range(-5, 5), z);
        yield return new WaitForSeconds(0.1f);
        source.Stop();
        Destroy(source);
    }

}
